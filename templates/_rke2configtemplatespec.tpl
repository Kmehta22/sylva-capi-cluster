{{- define "RKE2ConfigTemplateSpec" }}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}
{{- $machine_rke2_additional_user_data := index . 2 -}}

  {{/*********** Initialize the components of the RKE2ConfigTemplateSpec.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_kubelet_extra_args | include "base-RKE2ConfigTemplateSpec" | fromYaml }}
  {{- $infra := tuple $envAll $machine_rke2_additional_user_data | include (printf "%s-RKE2ConfigTemplateSpec" $envAll.Values.capi_providers.infra_provider) | fromYaml }}

agentConfig: {{ mergeOverwrite $base.agentConfig $infra.agentConfig (dict "kubelet" (dict "extraArgs" (concat $base.agentConfig.kubelet.extraArgs $infra.agentConfig.kubelet.extraArgs))) | toYaml | nindent 2 }}
preRKE2Commands:
  {{ $infra.preRKE2Commands | toYaml | nindent 2 }}
  {{ $base.preRKE2Commands | toYaml | nindent 2 }}
files: {{ concat $base.files $infra.files | toYaml | nindent 2 }}
{{- end }}
