{{- define "capd-RKE2ConfigTemplateSpec" }}
agentConfig:
  kubelet:
    extraArgs: []
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capo-RKE2ConfigTemplateSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      - "provider-id=openstack:///{{`{{ ds.meta_data.uuid }}`}}"
  # TODO: confirm agentConfig.nodeName for CAPO
  # nodeName: {}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capv-RKE2ConfigTemplateSpec" }}
agentConfig:
  kubelet:
    extraArgs: []
  nodeName: {{`"{{ ds.meta_data.hostname }}"`}}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
  - sleep 30 # fix to give OS time to become ready, per https://github.com/rancher-sandbox/cluster-api-provider-rke2/blob/main/samples/vmware/rke2configtemplate.yaml
files: []
{{- end }}

{{- define "capm3-RKE2ConfigTemplateSpec" }}
{{- $envAll := index . 0 }}
{{- $machine_rke2_additional_user_data := index . 1 -}}
agentConfig:
{{- if $machine_rke2_additional_user_data }}
  additionalUserData: 
    config: |{{ mergeOverwrite (deepCopy $envAll.Values.machine_deployment_default.rke2.additionalUserData) $machine_rke2_additional_user_data | toYaml | nindent 6 }}
{{- end }}
  kubelet:
    extraArgs:
      - provider-id=metal3://{{`{{ ds.meta_data.uuid }}`}}
  nodeName: {{`"{{ ds.meta_data.local_hostname }}"`}}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
  - sleep 30 # fix to give OS time to become ready, per https://github.com/rancher-sandbox/cluster-api-provider-rke2/blob/main/samples/metal3/sample-cluster.yaml
  - netplan apply
files: []
{{- end }}
