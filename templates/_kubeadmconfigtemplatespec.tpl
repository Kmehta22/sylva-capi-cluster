{{- define "KubeadmConfigTemplateSpec" -}}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}

  {{/*********** Initialize the components of the KubeadmConfigTemplate.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_kubelet_extra_args | include "base-KubeadmConfigTemplateSpec" | fromYaml }}
  {{- $infra := include (printf "%s-KubeadmConfigTemplateSpec" $envAll.Values.capi_providers.infra_provider) $envAll | fromYaml }}

joinConfiguration: {{ mergeOverwrite $base.joinConfiguration $infra.joinConfiguration | toYaml | nindent 2 }}
{{- if $envAll.Values.ntp }}
ntp: {{ mergeOverwrite $base.ntp $infra.ntp | toYaml | nindent 2 }}
{{- end }}
preKubeadmCommands:
  {{ $infra.preKubeadmCommands | toYaml | nindent 2 }}
  {{ $base.preKubeadmCommands | toYaml | nindent 2 }}
files: {{ concat $base.files $infra.files | toYaml | nindent 2 }}
users: {{ concat $base.users $infra.users | toYaml | nindent 2 }}
{{- end }}
