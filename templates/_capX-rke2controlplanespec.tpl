{{- define "capd-RKE2ControlPlaneSpec" }}
registrationMethod: internal-first
agentConfig:
  # TODO: test a rke2-capd deployment with "cis_profile: cis-1.23" to see what breaks
  # excluding from rke2-capd for now
  cisProfile: null
  kubelet:
    extraArgs:
      - "eviction-hard=nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%"
  ntp: null {{/* CAPD infra provider does not support NTP configuration for RKE2ControlPlane.spec.agentConfig */}}
serverConfig:
  tlsSan:
    - localhost
    - 127.0.0.1
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capo-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      - "provider-id=openstack:///{{`{{ ds.meta_data.uuid }}`}}"
serverConfig: {}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capv-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      - "--cloud-provider=external"
serverConfig: {}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capm3-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      - "provider-id=metal3://{{`{{ ds.meta_data.uuid }}`}}"
  nodeName: {{`'{{ ds.meta_data.local_hostname }}'`}}
preRKE2Commands:
  - netplan apply
  - sleep 30 # fix to give OS time to become ready
files: []
{{- end }}
