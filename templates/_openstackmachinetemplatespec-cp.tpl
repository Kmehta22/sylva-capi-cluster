{{ define "OpenStackMachineTemplateSpec-CP" }}
cloudName: capo_cloud
flavor: {{ .Values.capo.flavor_name }}
identityRef:
  kind: Secret
  name: {{ .Values.name }}-capo-cloud-config
image: {{ .Values.image }}
sshKeyName: {{ .Values.capo.ssh_key_name }}
serverGroupID: {{ .Values.capo.control_plane_servergroup_id }}
securityGroups:
  - name: default
  - name: {{ .Values.capo.control_plane_security_group_name }}
ports:
  - network:
      id: {{ .Values.capo.network_id }}
    allowedAddressPairs:
      - ipAddress: {{ .Values.cluster_external_ip }}
    {{- if .Values.bgp_lbs.metallb }}
      {{- range .Values.bgp_lbs.metallb.address_pools }}
        {{- range .addresses -}}
          {{- if regexMatch "^[0-9.]*-[0-9.]*$" . -}}
            {{- $range := split "-" . }}
            {{- $firstIPBits := split "." $range._0 -}}
            {{- $lastIPBits := split "." $range._1 }}
            {{- range untilStep ($firstIPBits._3 | int) ($lastIPBits._3 | add1 | int) 1 }}
      - ipAddress: {{ $firstIPBits._0 }}.{{ $firstIPBits._1 }}.{{ $firstIPBits._2 }}.{{ . }}
            {{- end -}}
          {{- else }}
            {{- if regexMatch "^[0-9.]*/[0-9.]*$" . }}
              {{- $range := split "/" . }}
              {{- $ip_add := $range._0 }}
              {{- if ne $ip_add $.Values.cluster_external_ip }}
      - ipAddress: {{ $ip_add }}
              {{- end }}
            {{- end }}
          {{- end }}
        {{- end }}
      {{- end }}
    {{- end }}
{{ if .Values.capo.rootVolume }}
rootVolume:
  diskSize: {{ .Values.capo.rootVolume.diskSize | int }}
  volumeType: {{ .Values.capo.rootVolume.volumeType }}
{{ end }}
{{ end }}
