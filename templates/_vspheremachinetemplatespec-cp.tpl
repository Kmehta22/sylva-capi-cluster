{{ define "VSphereMachineTemplateSpec-CP" }}
cloneMode: linkedClone
datacenter: {{ .Values.capv.dataCenter }}
datastore: {{ .Values.capv.dataStore }}
diskGiB: {{ .Values.capv.diskGiB }}
folder: {{ .Values.capv.folder }}
memoryMiB: {{ .Values.capv.memoryMiB }}
network:
  devices:
  - dhcp4: true
    networkName: {{ .Values.capv.network }}
numCPUs: {{ .Values.capv.numCPUs }}
resourcePool: {{ .Values.capv.resourcePool }}
server: {{ .Values.capv.server }}
storagePolicyName: {{ .Values.capv.storagePolicyName }}
template: {{ .Values.image }}
thumbprint: {{ .Values.capv.tlsThumbprint }}
{{ end }}
